import 'package:flutter/material.dart';
import 'package:tela_navegacao/pages/widgets/email_filed.dart';
import 'package:tela_navegacao/pages/widgets/password_filed.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final formMasterKey = GlobalKey<FormState>();
  bool disableLoginBtn = true;

  _checkLogin() {
    if (formMasterKey.currentState!.validate()) {
      Navigator.popAndPushNamed(context, 'secondpage');
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Form(
      key: formMasterKey,
      child: ListView(
        padding: const EdgeInsets.only(top: 0),
        physics: const BouncingScrollPhysics(),
        children: [
          const Logo(),
          _Titulo(),
          const SizedBox(height: 10),
          const EmailField(),
          const PasswordField(),
          _ForgotPassword(),
          const SizedBox(height: 40),
          Container(
            margin: const EdgeInsets.all(25),
            decoration: BoxDecoration(
              color: const Color.fromARGB(255, 39, 5, 71),
              borderRadius: BorderRadius.circular(10),
            ),
            child: TextButton(
              onPressed: _checkLogin,
              child: const Text('LOGIN',
                  style: TextStyle(color: Colors.white, fontSize: 18)),
            ),
          )
        ],
      ),
    ));
  }
}

class _ForgotPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(right: 25, top: 20),
      alignment: Alignment.centerRight,
      child: const Text('Forgot Password?'),
    );
  }
}

class _Titulo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Padding(
        padding: EdgeInsets.all(15.0),
        child: Center(
            child: Text(
          "Welcome Back!",
          style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700),
        )));
  }
}

class Logo extends StatelessWidget {
  const Logo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(60.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(100),
        child: Image.asset(
          'assets/images/raro.jpeg',
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
