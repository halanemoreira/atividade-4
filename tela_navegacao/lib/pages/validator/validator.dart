class Validator {
  static String? validateEmail(String? value) {
    final regexEmail = RegExp(
        r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])+");
    if (value == null || value.isEmpty) {
      return 'Preencha o Campo, Por favor!';
    }
    if (!value.contains(regexEmail)) {
      return 'E-mail inválido, digite um e-mail corretamente!';
    }
    return null;
  }

  static String? validatePassword(String? value) {
    if (value == null || value.isEmpty) {
      return 'Preencha o Campo, Por favor!';
    }
    return null;
  }
}
