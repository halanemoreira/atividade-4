import 'package:flutter/material.dart';
import '../validator/validator.dart';

class PasswordField extends StatefulWidget {
  const PasswordField({
    Key? key,
  }) : super(key: key);

  @override
  State<PasswordField> createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  String? password;

  final passwordcontrole = TextEditingController();

  bool showPassword = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(
            left: 30.0,
            top: 10.0,
            bottom: 10.0,
            right: 30.0,
          ),
          child: TextFormField(
            validator: (value) =>
                Validator.validatePassword(passwordcontrole.text),
            controller: passwordcontrole,
            obscuringCharacter: "*",
            onChanged: (password) {
              password = password;
            },
            textInputAction: TextInputAction.next,
            decoration: InputDecoration(
                errorMaxLines: 3,
                labelText: "Senha",
                border: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(90.0)),
                    borderSide: BorderSide(color: Colors.white24, width: 0.5)),
                prefixIcon: const Icon(Icons.key),
                suffixIcon: IconButton(
                  icon: Icon(
                      !showPassword ? Icons.visibility : Icons.visibility_off),
                  onPressed: () {
                    setState(() {
                      showPassword = !showPassword;
                    });
                  },
                )),
            obscureText: showPassword,
          ),
        ),
      ],
    );
  }
}
