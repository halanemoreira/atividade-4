import 'package:flutter/material.dart';

import '../validator/validator.dart';

class EmailField extends StatefulWidget {
  const EmailField({
    Key? key,
  }) : super(key: key);

  @override
  State<EmailField> createState() => EmailFieldState();
}

class EmailFieldState extends State<EmailField> {
  final emailcontrole = TextEditingController();
  String email = "";
  @override
  void dispose() {
    emailcontrole.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 30.0,
        top: 10.0,
        bottom: 10.0,
        right: 30.0,
      ),
      child: TextFormField(
        validator: (value) => Validator.validateEmail(emailcontrole.text),
        controller: emailcontrole,
        onChanged: (value) {
          setState(() {
            email = value;
          });
        },
        keyboardType: TextInputType.emailAddress,
        textInputAction: TextInputAction.next,
        decoration: const InputDecoration(
          prefixIcon: Icon(Icons.email),
          labelText: "E-mail",
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(90.0)),
              borderSide: BorderSide(color: Colors.white24, width: 0.5)),
        ),
      ),
    );
  }
}
