import 'package:flutter/material.dart';
import 'package:tela_navegacao/pages/route_generator.dart';

class Data {
  final List _data = [
    {
      "id": 1,
      "title": "FLUTTER",
      "description":
          "A framework, an open source development kit created by Google.",
      "route": 'thirdpage'
    },
    {
      "id": 2,
      "title": "C#",
      "description":
          "NET is a free and open source framework for Windows, Linux and macOS .",
      "route": 'thirdpage'
    },
    {
      "id": 3,
      "title": "Python",
      "description": "Coming soon!",
      "route": 'invalid-route'
    },
  ];

  int getId(int index) {
    return _data[index]["id"];
  }

  String getTitle(int index) {
    return _data[index]["title"];
  }

  String getDescription(int index) {
    return _data[index]["description"];
  }

  String getRoute(int index) {
    return _data[index]["route"];
  }

  int getLength() {
    return _data.length;
  }
}

class SecondPage extends StatefulWidget {
  const SecondPage({Key? key}) : super(key: key);
  @override
  State<SecondPage> createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  final Data _data = Data();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 39, 5, 71),
      body: ListView.builder(
        padding: const EdgeInsets.only(top: 90),
        itemCount: _data.getLength(),
        itemBuilder: _itemBuilder,
      ),
    );
  }

  Widget _itemBuilder(BuildContext context, int index) {
    return InkWell(
        child: Center(
          child: Card(
            margin: const EdgeInsets.only(top: 30),
            elevation: 8,
            shadowColor: Color.fromARGB(255, 238, 243, 238),
            child: SizedBox(
                width: 340,
                height: 220,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        textAlign: TextAlign.center,
                        _data.getTitle(index),
                        style: const TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.w900,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        textAlign: TextAlign.center,
                        _data.getDescription(index),
                        style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w400,
                          color: Color.fromARGB(255, 107, 98, 98),
                        ),
                      ),
                    ],
                  )),
                )),
          ),
        ),
        onTap: () => {
              Navigator.pushNamed(
                context,
                _data.getRoute(index),
                arguments: ScreenArguments(index.toString()),
              )
            });
  }
}
