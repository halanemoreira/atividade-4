import 'package:flutter/material.dart';
import 'package:tela_navegacao/src/raro_system/atoms/text/h_text_h1.dart';

import '../src/raro_system/atoms/text/h_text_h2.dart';

class Data {
  final List _data = [
    {
      "id": 1,
      "title": "FLUTTER",
      "description":
          "A framework, an open source development kit created by Google.",
    },
    {
      "id": 2,
      "title": "C#",
      "description":
          "NET is a free and open source framework for Windows, Linux and macOS .",
    },
  ];

  int getId(int index) {
    return _data[index]["id"];
  }

  String getTitle(int index) {
    return _data[index]["title"];
  }

  String getDescription(int index) {
    return _data[index]["description"];
  }

  int getLength() {
    return _data.length;
  }
}

class ThirdPage extends StatefulWidget {
  const ThirdPage({Key? key, required this.id}) : super(key: key);

  final String id;

  @override
  State<ThirdPage> createState() => _ThirdPageState(id);
}

class _ThirdPageState extends State<ThirdPage> {
  final Data _data = Data();
  final String id;

  _ThirdPageState(this.id);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 187, 155, 218),
      appBar: AppBar(title: Text('Couse ID $id')),
      body: ListView.builder(
        padding: const EdgeInsets.only(top: 90),
        itemCount: _data.getLength(),
        itemBuilder: _itemBuilder,
      ),
    );
  }

  Widget _itemBuilder(BuildContext context, int index) {
    return InkWell(
      child: Center(
        child: Card(
          margin: const EdgeInsets.only(top: 30),
          elevation: 8,
          shadowColor: Color.fromARGB(255, 238, 243, 238),
          child: SizedBox(
              width: 340,
              height: 220,
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Center(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    index == 1
                        ? HtextH1(text: _data.getTitle(index))
                        : HtextH2(text: _data.getTitle(index)),
                    Text(
                      textAlign: TextAlign.center,
                      _data.getDescription(index),
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        color: Color.fromARGB(255, 107, 98, 98),
                      ),
                    ),
                  ],
                )),
              )),
        ),
      ),
    );
  }
}
