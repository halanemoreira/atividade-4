import 'package:flutter/material.dart';

class ErrorPage extends StatelessWidget {
  const ErrorPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 255, 47, 0),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: const Text("404",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 90,
                    fontWeight: FontWeight.w900,
                    color: Color.fromARGB(255, 247, 243, 243),
                  )),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: const Text("page not found",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w400,
                    color: Color.fromARGB(255, 247, 243, 243),
                  )),
            ),
            ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, 'secondpage');
                },
                child: const Text("Voltar"))
          ]),
    );
  }
}
