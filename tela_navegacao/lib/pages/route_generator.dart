import 'package:flutter/material.dart';
import 'package:tela_navegacao/pages/error_page.dart';
import 'package:tela_navegacao/pages/login_page.dart';
import 'package:tela_navegacao/pages/second_page.dart';
import 'package:tela_navegacao/pages/third_page.dart';

class ScreenArguments {
  final String id;
  ScreenArguments(this.id);
}

class RouteGenerator {
  static Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case "login":
        return MaterialPageRoute(builder: (context) => const LoginPage());
      case "secondpage":
        return MaterialPageRoute(builder: (context) => const SecondPage());
      case "thirdpage":
        final args = settings.arguments as ScreenArguments;
        return MaterialPageRoute(
            builder: (context) => ThirdPage(
                  id: args.id,
                ));
      default:
        return MaterialPageRoute(builder: (context) => const ErrorPage());
    }
  }
}
